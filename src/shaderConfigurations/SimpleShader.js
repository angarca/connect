
const configuration = {
  vertexShaderUrl: './shaders/simple.v.glsl',
  fragmentShaderUrl: './shaders/simple.f.glsl',
  attributes: [ 'aVertexPosition', 'aVertexColor' ],
  uniforms: [ 'uProjectionMatrix', 'uModelViewMatrix' ],
};

export default configuration;
