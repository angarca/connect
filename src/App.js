import React, { Component } from 'react';
import QueryString from 'query-string';
import BasicCanvas from './layouts/BasicCanvas/BasicCanvas';
import InteractiveText from './layouts/InteractiveText/InteractiveText';
import SimpleRotatingSquare from './gameLogics/SimpleRotatingSquare';
import SimpleColoredCube from './gameLogics/SimpleColoredCube';
import ColoredCubeWithCameraMovement from './gameLogics/ColoredCubeWithCameraMovement';

export default class App extends Component {

  constructor( props) {
    super( props );
    this.ref = React.createRef();
    this.gameLogic = null;
    this.Layout = BasicCanvas;
    this.extraProps = {};
    const query = QueryString.parse( window.location.search );
    if( 'rotatingSquare' in query ) this.gameLogic = new SimpleRotatingSquare();
    if( 'coloredCube' in query ) this.gameLogic = new SimpleColoredCube();
    if( 'cubeWithMovement' in query ) this.gameLogic = new ColoredCubeWithCameraMovement();
    if( !this.gameLogic || 'interactiveText' in query ) {
      this.Layout = InteractiveText;
      this.gameLogic = new ColoredCubeWithCameraMovement();
    };
    if( 'script' in query ) {
      this.extraProps.script = query.script;
    }
  }

  render() {
    return <this.Layout {...this.extraProps} ref={this.ref}/>;
  }

  componentDidMount() {
    if( this.ref.current && this.gameLogic ) {
      this.gameLogic.setGlContext( this.ref.current.getGlContext() );
      this.gameLogic.start();
    }
  }

}
