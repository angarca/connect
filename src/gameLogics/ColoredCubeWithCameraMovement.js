
import Camera from '../helpers/Camera';
import { generateShaderProgram, setBufferData, drawScene } from '../helpers/webgl';
import shaderConfig from '../shaderConfigurations/SimpleShader';
import { vertices, colors, indices } from '../geometries/SimpleColoredCube';

export default class ColoredCubeWithCameraMovement {

  constructor({ glcontext = null } = {}) {
    this.glcontext = glcontext;
    this.camera = new Camera({ glcontext });

    this.lastFrameTime = 0.0;
    this.cubeRotation = 0.0;
    this.mouseLeftClicked = false;
    this.mouseRightClicked = false;
    this.mouseBothOrWheelClicked = false;
    this.mouseLastPosition = { x: null, y: null };

    this.draw = this.draw.bind( this );
    this.onKeyDown = this.onKeyDown.bind( this );
    window.addEventListener( 'keydown', this.onKeyDown );

    this.onMouseEvent = this.onMouseEvent.bind( this );
    this.setMouseEventListeners = this.setMouseEventListeners.bind( this );
    this.eventListeners = [
      // { event: 'keydown', listener: this.onKeyDown },
      { event: 'mousedown', listener: this.onMouseEvent },
      { event: 'mouseup', listener: this.onMouseEvent },
      { event: 'mousemove', listener: this.onMouseEvent },
      { event: 'wheel', listener: this.onMouseEvent },
    ];
    this.setMouseEventListeners();
  }

  setMouseEventListeners() {
    if( this.glcontext ) {
      const canvas = this.glcontext.canvas;
      if( this.lastCanvas ) {
        this.eventListeners.forEach( ({ event, listener }) => {
          this.lastCanvas.removeEventListener( event, listener );
        });
      }
      this.eventListeners.forEach( ({ event, listener }) => {
        canvas.addEventListener( event, listener );
      });
      canvas.oncontextmenu = e => false ;
      this.lastCanvas = canvas;
    }
  }

  onKeyDown( event ) {
    switch( event.key.toLowerCase() ) {
      case 'arrowup':
      case 'w':
        this.camera.translateViewMatrix([ 0.0, -1.0, 0.0 ]);
        break;
      case 'arrowdown':
      case 's':
        this.camera.translateViewMatrix([ 0.0, 1.0, 0.0 ]);
        break;
      case 'arrowleft':
      case 'a':
        this.camera.translateViewMatrix([ 1.0, 0.0, 0.0 ]);
        break;
      case 'arrowright':
      case 'd':
        this.camera.translateViewMatrix([ -1.0, 0.0, 0.0 ]);
        break;
      default:;
    }
  }

  onMouseEvent( event ) {
    // event.preventDefault();    // REVIEW
    switch( event.buttons ) {
      case 0:
        this.mouseLeftClicked = false;
        this.mouseRightClicked = false;
        this.mouseBothOrWheelClicked = false;
        break;
      case 1:
        this.mouseLeftClicked = true;
        break;
      case 2:
        this.mouseRightClicked = true;
        break;
      case 3:
        this.mouseLeftClicked = true;
        this.mouseRightClicked = true;
        break;
      case 4:
        this.mouseBothOrWheelClicked = true;
        break;
      default:;
    }
    if( this.mouseLastPosition.x && this.mouseLastPosition.y ) {
      if( this.mouseLeftClicked ) {
        this.camera.translateViewMatrix([ event.movementX / 100.0, event.movementY / -100.0, 0.0 ]);
      }
      if( this.mouseRightClicked ) {
        const x = event.movementX;
        const y = event.movementY;
        const absoluteMovement = Math.hypot( x, y );
        this.camera.rotateViewMatrix( absoluteMovement / -100.0, [ y, x, 0 ]);
      }
      if( this.mouseBothOrWheelClicked ) {
        this.camera.translateViewMatrix([ 0.0, 0.0, event.movementY / -100.0 ]);
        this.camera.rotateViewMatrix( event.movementX / -100.0, [ 0, 0, 1 ]);
      }
      if( event.deltaY ) {
        this.camera.translateViewMatrix([ 0.0, 0.0, event.deltaY / -10.0 ]);
      }
    }

    this.mouseLastPosition = { x: event.clientX, y: event.clientY };
  }

  setGlContext( glcontext ) {
    this.glcontext = glcontext;
    this.camera.setGlContext( glcontext );
    this.setMouseEventListeners();
  }

  async start() {
    const gl = this.glcontext;
    this.shaderProgramInfo = null;

    // Solo continuar si WebGL esta disponible y trabajando
    if( gl && shaderConfig ) {
      this.shaderProgramInfo = await generateShaderProgram( gl, shaderConfig );
      const shaderValues = {
        aVertexPosition: vertices,
        aVertexColor: colors,
      }
      shaderValues.aVertexPosition.options = { numComponents: 3 };

      const noAllocatedBuffers = {
        indices,
      }
      noAllocatedBuffers.indices.options = { target: 'ELEMENT_ARRAY_BUFFER', typeConstructor: Uint16Array };

      this.shaderProgramInfo = setBufferData( gl, this.shaderProgramInfo, shaderValues, noAllocatedBuffers );
      requestAnimationFrame( this.draw );
    }
  }

  draw( now ) {
    const gl = this.glcontext;

    this.cubeRotation += ( now - this.lastFrameTime ) / 1000.0;
    this.lastFrameTime = now;

    const shaderValues = {
      uProjectionMatrix: this.camera.getProjectionMatrix(),
      uModelViewMatrix: this.camera.getModelViewMatrix({ rotate: { pitch: this.cubeRotation, yaw: this.cubeRotation *.7 } }),
    };

    const noAllocatedBuffers = {
      indices,
    };

    drawScene( gl, this.shaderProgramInfo, shaderValues, { drawFunction: 'drawElements', count: 36 }, noAllocatedBuffers );

    requestAnimationFrame( this.draw );
  }

}
