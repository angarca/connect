
import Camera from '../helpers/Camera';
import { generateShaderProgram, drawScene } from '../helpers/webgl';
import shaderConfig from '../shaderConfigurations/SimpleShader';

export default class SimpleRotatingSquare {

  constructor({ glcontext = null } = {}) {
    this.glcontext = glcontext;
    this.camera = new Camera({ glcontext });

    this.lastFrameTime = 0.0;
    this.squareRotation = 0.0;

    this.draw = this.draw.bind( this );
  }

  setGlContext( glcontext ) {
    this.glcontext = glcontext;
    this.camera.setGlContext( glcontext );
  }

  async start() {
    const gl = this.glcontext;
    this.shaderProgramInfo = null;

    // Solo continuar si WebGL esta disponible y trabajando
    if( gl && shaderConfig ) {
      this.shaderProgramInfo = await generateShaderProgram( gl, shaderConfig);
      requestAnimationFrame( this.draw );
    }
  }

  draw( now ) {
    const gl = this.glcontext;

    this.squareRotation += ( now - this.lastFrameTime ) / 1000.0;
    this.lastFrameTime = now;

    const shaderValues = {
      aVertexPosition: [
        1.0,  1.0,
        -1.0,  1.0,
        1.0, -1.0,
        -1.0, -1.0,
      ],
      aVertexColor: [
        1.0,  1.0,  1.0,  1.0,    // blanco
        1.0,  0.0,  0.0,  1.0,    // rojo
        0.0,  1.0,  0.0,  1.0,    // verde
        0.0,  0.0,  1.0,  1.0     // azul
      ],
      uProjectionMatrix: this.camera.getProjectionMatrix(),
      uModelViewMatrix: this.camera.getModelViewMatrix({ rotate: { roll: this.squareRotation } }),
    };
    shaderValues.aVertexPosition.options = { numComponents: 2 };

    drawScene( gl, this.shaderProgramInfo, shaderValues, { vertexCount: 4 } );

    requestAnimationFrame( this.draw );
  }

}
