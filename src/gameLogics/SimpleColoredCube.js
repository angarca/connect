
import Camera from '../helpers/Camera';
import { generateShaderProgram, drawScene } from '../helpers/webgl';
import shaderConfig from '../shaderConfigurations/SimpleShader';
import { vertices, colors, indices } from '../geometries/SimpleColoredCube';

export default class SimpleColoredCube {

  constructor({ glcontext = null } = {}) {
    this.glcontext = glcontext;
    this.camera = new Camera({ glcontext });

    this.lastFrameTime = 0.0;
    this.cubeRotation = 0.0;

    this.draw = this.draw.bind( this );
  }

  setGlContext( glcontext ) {
    this.glcontext = glcontext;
    this.camera.setGlContext( glcontext );
  }

  async start() {
    const gl = this.glcontext;
    this.shaderProgramInfo = null;

    // Solo continuar si WebGL esta disponible y trabajando
    if( gl && shaderConfig ) {
      this.shaderProgramInfo = await generateShaderProgram( gl, shaderConfig);
      requestAnimationFrame( this.draw );
    }
  }

  draw( now ) {
    const gl = this.glcontext;

    this.cubeRotation += ( now - this.lastFrameTime ) / 1000.0;
    this.lastFrameTime = now;

    const shaderValues = {
      aVertexPosition: vertices,
      aVertexColor: colors,
      uProjectionMatrix: this.camera.getProjectionMatrix(),
      uModelViewMatrix: this.camera.getModelViewMatrix({ rotate: { pitch: this.cubeRotation, yaw: this.cubeRotation *.7 } }),
    };
    shaderValues.aVertexPosition.options = { numComponents: 3 };

    const extraBuffers = {
      indices,
    };
    extraBuffers.indices.options = { target: 'ELEMENT_ARRAY_BUFFER', typeConstructor: Uint16Array };

    drawScene( gl, this.shaderProgramInfo, shaderValues, { drawFunction: 'drawElements', count: 36 }, extraBuffers );

    requestAnimationFrame( this.draw );
  }

}
