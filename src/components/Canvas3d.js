import React, { Component } from 'react';
import { getWebglContext } from '../helpers/webgl';

export default class Canvas3d extends Component {

  constructor( props) {
    super( props);
    this.glcanvas = React.createRef();
    this.glcontext = null;
  }

  render() {
    const { width, height } = this.props.style || {};
    return (
      <canvas id={this.props.id} {...{width,height}} ref={this.glcanvas}>
       Tu navegador parece no soportar el elemento HTML5 <code>&lt;canvas&gt;</code>.
      </canvas>
    );
  }

  getGlContext() {
    return this.glcontext;
  }

  componentDidMount() {
    this.glcontext = getWebglContext( this.glcanvas.current );
  }

  componentDidUpdate() {
    const gl = this.glcontext;
    if( gl) gl.viewport( 0, 0, this.glcanvas.current.width, this.glcanvas.current.height);
  }

}
