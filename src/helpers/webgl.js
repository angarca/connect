
export function getWebglContext( canvas) {
  let gl = null;

  try {
    // Tratar de tomar el contexto estandar. Si falla, retornar al experimental.
    gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
  } catch(e) {}

  // Si no tenemos ningun contexto GL, date por vencido ahora
  if (!gl) {
    alert("Imposible inicializar WebGL. Tu navegador puede no soportarlo.");
    gl = null;
  }

  return gl;
}

//
// creates a shader of the given type, uploads the source and
// compiles it.
//
async function loadShader( gl, type, source) {
  const shader = gl.createShader(type);

  // Request the text source from a remote url
  const shaderText = await fetch( source).then( res => res.text());
  // Send the source to the shader object
  gl.shaderSource( shader, shaderText);
  // Compile the shader program
  gl.compileShader(shader);
  // See if it compiled successfully
  if( !gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    alert( `An error occurred compiling the shaders: ${gl.getShaderInfoLog(shader)}`);
    gl.deleteShader(shader);
    return null;
  }

  return shader;
}

//
// Initialize a shader program, so WebGL knows how to draw our data
//
async function initShaderProgram( gl, vsSource, fsSource) {
  const shaderProgram = gl.createProgram();
  let vertexShader = null, fragmentShader = null;
  if( vsSource) {
    vertexShader = await loadShader( gl, gl.VERTEX_SHADER, vsSource);
    gl.attachShader( shaderProgram, vertexShader);
  }
  if( fsSource) {
    fragmentShader = await loadShader( gl, gl.FRAGMENT_SHADER, fsSource);
    gl.attachShader( shaderProgram, fragmentShader);
  }
  gl.linkProgram( shaderProgram);

  // If creating the shader program failed, alert
  if( !gl.getProgramParameter( shaderProgram, gl.LINK_STATUS)) {
    alert( `Unable to initialize the shader program: ${gl.getProgramInfoLog(shaderProgram)}`);
    return null;
  }

  return shaderProgram;
}

function getAttribLocations( gl, program, shaderConfig ) {
  return shaderConfig.attributes.map( attribute => {
    return {
      key: attribute,
      location: gl.getAttribLocation( program, attribute ),
    };
  });
}

function getUniformLocations( gl, program, shaderConfig ) {
  return shaderConfig.uniforms.map( uniform => {
    return {
      key: uniform,
      location: gl.getUniformLocation( program, uniform ),
    };
  });
}

export async function generateShaderProgram( gl, shaderConfig) {
  const program = await initShaderProgram( gl, shaderConfig.vertexShaderUrl, shaderConfig.fragmentShaderUrl);
  return {
    program,
    attribLocations: getAttribLocations( gl, program, shaderConfig),
    uniformLocations: getUniformLocations( gl, program, shaderConfig),
  };
}

function createAndBufferData( gl, data, options = {} ) {
  const { target = 'ARRAY_BUFFER', typeConstructor = Float32Array } = options;
  Object.assign( options, { target } );
  const buffer = gl.createBuffer();
  gl.bindBuffer( gl[ target ], buffer);
  gl.bufferData( gl[ target ], new typeConstructor(data), gl.STATIC_DRAW);
  return { buffer, options };
}

export function setBufferData( gl, programInfo, shaderValues, noAllocatedBuffers = {} ) {
  let buffersTable = programInfo.buffersTable;
  if( !buffersTable ) programInfo.buffersTable = buffersTable = {};
  programInfo.attribLocations.forEach( ({ key, location }) => {
    if( shaderValues[ key ] ) {
      const values = shaderValues[ key ];
      const options = values.options;
      buffersTable[ key ] = createAndBufferData( gl, values, options );
    }
  });
  Object.keys( noAllocatedBuffers ).forEach( key => {
    const values = noAllocatedBuffers[ key ];
    const options = values.options;
    buffersTable[ key ] = createAndBufferData( gl, values, options );
  });
  return programInfo;
}

function clearContext( gl) {
  gl.clearColor( 0.0, 0.0, 0.0, 1.0);                      // Establecer el color base en negro, totalmente opaco
  gl.clearDepth( 1.0);                                     // Clear everything
  gl.enable( gl.DEPTH_TEST);                               // Habilitar prueba de profundidad
  gl.depthFunc( gl.LEQUAL);                                // Objetos cercanos opacan objetos lejanos

  // Clear the canvas before we start drawing on it.
  gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);    // Limpiar el buffer de color asi como el de profundidad
}

function setBufferAttrib( gl, buffer, position, { target = 'ARRAY_BUFFER', numComponents = 4, type = gl.FLOAT, normalize = false, stride = 0, offset = 0 } = {} ) {
  gl.bindBuffer( gl[ target ], buffer);
  gl.vertexAttribPointer( position, numComponents, type, normalize, stride, offset );
  gl.enableVertexAttribArray( position);
}

function fullSetBufferAttribs( gl, attribLocations = [], shaderValues = {}, buffersTable = {} ) {
  attribLocations.forEach( ({ key, location }) => {
    let loaded = false;
    if( buffersTable && buffersTable[ key ] ) {
      const { buffer, options } = buffersTable[ key ];
      if( !options || !options.autoRefresh || !shaderValues[ key ] ) {
        setBufferAttrib( gl, buffer, location, options );
        loaded = true;
      }
    }
    if( shaderValues[ key ] ) {
      const values = shaderValues[ key ];
      const options = values.options;
      if( !loaded || (options && options.forceRefresh) ) {
        const { buffer } = createAndBufferData( gl, values, options );
        setBufferAttrib( gl, buffer, location, options );
      }
    }
  });
}

function fullBindNoAllocatedBuffers( gl, noAllocatedBuffers = {}, buffersTable = {} ) {
  Object.keys( noAllocatedBuffers).forEach( key => {
    let binded = false;
    if( buffersTable && buffersTable[ key ] ) {
      const { buffer, options } = buffersTable[ key ];
      if( !options || !options.autoRefresh ) {
        gl.bindBuffer( gl[ options.target || 'ELEMENT_ARRAY_BUFFER' ], buffer);
        binded = true;
      }
    }
    const values = noAllocatedBuffers[ key ];
    const options = values.options;
    if( !binded || (options && options.forceRefresh) ) {
      createAndBufferData( gl, values, options);
    }
  });
}

export function drawScene( gl, programInfo, shaderValues, drawOptions = {}, noAllocatedBuffers = {} ) {

  clearContext( gl);

  fullSetBufferAttribs( gl, programInfo.attribLocations, shaderValues, programInfo.buffersTable );
  fullBindNoAllocatedBuffers( gl, noAllocatedBuffers, programInfo.buffersTable );

  // Tell WebGL to use our program when drawing
  gl.useProgram( programInfo.program);

  programInfo.uniformLocations.forEach( ({ key, location }) => {
    if( shaderValues[ key ] ) {
      gl.uniformMatrix4fv( location, false, shaderValues[ key ] );
    }
  });

  switch( drawOptions.drawFunction ) {
    default:
    case 'drawArrays':
      (({ mode = 'TRIANGLE_STRIP', offset = 0, vertexCount = 1 }) => {
        gl.drawArrays( gl[ mode ], offset, vertexCount );
      })( drawOptions );
      break;
    case 'drawElements':
      (({ mode = 'TRIANGLES', count = 1, type = 'UNSIGNED_SHORT', offset = 0 }) => {
        gl.drawElements( gl[ mode ], count, gl[ type ], offset );
      })( drawOptions );
      break;
  }

}
