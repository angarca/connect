
import { mat4 } from 'gl-matrix';

export default class Camera {

  constructor({ glcontext = null, fieldOfView = 45 * Math.PI / 180, zNear = 0.1, zFar = 100.0 } = {}) {
    this.glcontext = glcontext;
    this.fieldOfView = fieldOfView;   // in radians
    this.zNear = zNear;
    this.zFar = zFar;

    this.viewMatrix = mat4.create();
    mat4.fromTranslation( this.viewMatrix, [ 0.0, 0.0, -6.0] );
  }

  setGlContext( glcontext ) {
    this.glcontext = glcontext;
  }

  translateViewMatrix([ x, y, z ]) {
    const translationMatrix = mat4.create();
    mat4.fromTranslation( translationMatrix, [ x, y , z ] );
    mat4.multiply( this.viewMatrix, translationMatrix, this.viewMatrix );
  }

  rotateViewMatrix( angle = 0.0, axis = [ 0, 0, 0 ] ) {
    const rotationMatrix = mat4.create();
    mat4.fromRotation( rotationMatrix, angle, axis );
    mat4.multiply( this.viewMatrix, rotationMatrix, this.viewMatrix );
  }

  getProjectionMatrix({ glcontext = null, fieldOfView = null, zNear = null, zFar = null } = {}) {
    const Lgl = glcontext || this.glcontext;
    const LfieldOfView = fieldOfView || this.fieldOfView;
    const Laspect = Lgl.canvas.clientWidth / Lgl.canvas.clientHeight;
    const LzNear = zNear || this.zNear;
    const LzFar = zFar || this.zFar;

    const projectionMatrix = mat4.create();
    mat4.perspective(
      projectionMatrix,
      LfieldOfView,
      Laspect,
      LzNear,
      LzFar,
    );

    return projectionMatrix;
  }

  getModelViewMatrix({ translate = {}, rotate = {}, scale = {} } = {}) {
    const { x: xTranslation = 0.0, y: yTranslation = 0.0, z: zTranslation = 0.0 } = translate;
    const { pitch: xRotation = 0.0, yaw: yRotation = 0.0, roll: zRotation = 0.0 } = rotate;
    const { x: xScale = 1.0, y: yScale = 1.0, z: zScale = 1.0 } = scale;

    const modelViewMatrix = mat4.clone( this.viewMatrix );
    mat4.scale(
      modelViewMatrix,
      modelViewMatrix,
      [
        xScale,
        yScale,
        zScale,
      ],
    );
    mat4.translate(
      modelViewMatrix,     // destination matrix
      modelViewMatrix,     // matrix to translate
      [                    // amount to translate
        xTranslation,
        yTranslation,
        zTranslation,
      ],
    );
    mat4.rotate(
      modelViewMatrix,  // destination matrix
      modelViewMatrix,  // matrix to rotate
      yRotation,        // amount to rotate in radians
      [0, 1, 0],        // axis to rotate around
    );
    mat4.rotate(
      modelViewMatrix,  // destination matrix
      modelViewMatrix,  // matrix to rotate
      xRotation,        // amount to rotate in radians
      [1, 0, 0],        // axis to rotate around
    );
    mat4.rotate(
      modelViewMatrix,  // destination matrix
      modelViewMatrix,  // matrix to rotate
      zRotation,        // amount to rotate in radians
      [0, 0, 1],        // axis to rotate around
    );

    return modelViewMatrix;
  }

}
