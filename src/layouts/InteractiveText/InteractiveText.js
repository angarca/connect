import React, { Component } from 'react';
import Canvas3d from '../../components/Canvas3d';
import './InteractiveText.css';

export default class BasicCanvas extends Component {

  constructor( props) {
    super( props);
    this.state = {
      style: {
        width: window.innerWidth,
        height: window.innerHeight,
      },
      script: [],
      scriptPosition: 0,
    };
    this.clickToNext = false;
    this.mainAudio = '';
    this.canvas3d = React.createRef();
    this.mainAudioComponent = React.createRef();
    this.secondaryAudioComponent = React.createRef();
    this.mainAudioVolume = 1.0;
    this.loopStart = 0.0;
    this.loopEnd = Infinity;
    this.secondaryAudioVolume = 1.0;
    this.resizeListener = event => {
      const newSize = { width: window.innerWidth, height: window.innerHeight};
      this.setState({ style: Object.assign( {}, this.state.style, newSize ) });
    };
    this.clickListener = event => {
      if( this.clickToNext ) this.next();
    }
    this.timeListener = () => {
      const audio = this.mainAudioComponent.current;
      if( audio && audio.currentTime >= this.loopEnd ) {
        audio.currentTime = this.loopStart;
      }
    }
    this.alive = true;
    this.timeLoop = () => !this.alive || (this.timeListener() && false) || setTimeout( this.timeLoop, 0);
    this.timeLoop();
  }

  getCurrentConf() {
    const script = Array.isArray( this.state.script ) ? this.state.script : [];
    return script[ this.state.scriptPosition ] || {};
  }

  next( overrideNext = null ) {
    const messageConf = this.getCurrentConf();
    const scriptPosition = this.state.scriptPosition + (overrideNext || messageConf.next || 1);
    this.setState({ scriptPosition });
  }

  async loadScript( scriptUrl = './scripts/main.json' ) {
    const script = await fetch( scriptUrl ).then( response => response.json() );
    this.setState({ script });
  }

  processMessageConf( messageConf ) {
    this.clickToNext = false;
    const options = [];
    if( messageConf.type ) switch( messageConf.type ) {
      case 'click':
        this.clickToNext = true;
        break;
      case 'sound':
        let audioVolumeAttrib = 'secondaryAudioVolume';
        if( messageConf.mode === 'loop' ) {
          this.mainAudio = messageConf.fileName;
          audioVolumeAttrib = 'mainAudioVolume';
        }
        if( messageConf.volume ) {
          this[ audioVolumeAttrib ] = messageConf.volume;
        }
        if( messageConf.loopStart ) {
          this.loopStart = messageConf.loopStart;
        }
        if( messageConf.loopEnd ) {
          this.loopEnd = messageConf.loopEnd;
        }
      case 'time':
        const seconds = messageConf.seconds || 0;
        setTimeout( () => this.next(), seconds * 1000 );
        break;
      case 'select':
        const answers = Array.isArray( messageConf.answers ) ? messageConf.answers : [];
        answers.forEach( answer => {
          options.push({
            message: answer.message || '',
            onClick: event => {
              event.target.blur();
              this.next( answer.next );
            },
          });
        });
        break;
      default:
        console.warn( `unrecognized message type ${messageConf.type}` );
    }
    const message = messageConf.message || '';
    const audioSrc = messageConf.sound || '';
    return { message, audioSrc, options };
  }

  render() {
    const messageConf = this.getCurrentConf();
    const { message, audioSrc, options } = this.processMessageConf( messageConf );
    return (
      <div>
        { this.mainAudio ? <audio className='audio' src={this.mainAudio} autoPlay loop ref={this.mainAudioComponent}/> : '' }
        { audioSrc ? <audio className='audio' src={audioSrc} autoPlay ref={this.secondaryAudioComponent}/> : '' }
        <p id='message'>{ message }</p>
        <div id='options'>
          { options.map( (option,i) => <button key={i} onClick={option.onClick}>{ option.message }</button> ) }
        </div>
        <Canvas3d style={this.state.style} ref={this.canvas3d}/>
      </div>
    );
  }

  getGlContext() {
    const canvas3d = this.canvas3d.current;
    const glcontext = canvas3d ? canvas3d.getGlContext() : null;
    return glcontext;
  }

  setAudioVolumes() {
    const mainAudio = this.mainAudioComponent.current;
    const secondaryAudio = this.secondaryAudioComponent.current;
    if( mainAudio ) {
      mainAudio.volume = this.mainAudioVolume;
    }
    if( secondaryAudio ) {
      secondaryAudio.volume = this.secondaryAudioVolume;
    }
  }

  componentDidMount() {
    window.addEventListener( 'resize', this.resizeListener);
    window.addEventListener( 'mousedown', this.clickListener);
    this.loadScript( this.props.script );
    this.setAudioVolumes();
  }

  componentDidUpdate() {
    this.setAudioVolumes();
  }

  componentWillUnmount() {
    window.removeEventListener( 'resize', this.resizeListener);
    window.removeEventListener( 'mousedown', this.clickListener);
  }

}
