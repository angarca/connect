import React, { Component } from 'react';
import Canvas3d from '../../components/Canvas3d';

export default class EmptyCanvas extends Component {

  constructor( props) {
    super( props);
    this.state = {
      style: {
        width: window.innerWidth,
        height: window.innerHeight,
      },
    };
    this.resizeListener = event => {
      const newSize = { width: window.innerWidth, height: window.innerHeight};
      this.setState({ style: Object.assign( {}, this.state.style, newSize ) });
    };
  }

  render() {
    return <Canvas3d style={this.state.style}/>;
  }

  componentDidMount() {
    window.addEventListener( 'resize', this.resizeListener);
  }

  componentWillUnmount() {
    window.removeEventListener( 'resize', this.resizeListener);
  }
  
}
