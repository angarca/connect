import React, { Component } from 'react';
import Canvas3d from '../../components/Canvas3d';

export default class BasicCanvas extends Component {

  constructor( props) {
    super( props);
    this.state = {
      style: {
        width: window.innerWidth,
        height: window.innerHeight,
      },
    };
    this.canvas3d = React.createRef();
    this.resizeListener = event => {
      const newSize = { width: window.innerWidth, height: window.innerHeight};
      this.setState({ style: Object.assign( {}, this.state.style, newSize ) });
    };
  }

  render() {
    return <Canvas3d style={this.state.style} ref={this.canvas3d}/>;
  }

  getGlContext() {
    const canvas3d = this.canvas3d.current;
    const glcontext = canvas3d ? canvas3d.getGlContext() : null;
    return glcontext;
  }

  componentDidMount() {
    window.addEventListener( 'resize', this.resizeListener);
  }

  componentWillUnmount() {
    window.removeEventListener( 'resize', this.resizeListener);
  }

}
